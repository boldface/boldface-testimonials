<?php
/**
 * Plugin Name: Boldface Testimonials
 * Plugin URI: http://boldfacedesign.com/plugins/boldface-testimonials/
 * Description: Adds a [boldface_testimonials] shortcode
 * Version: 1.0
 * Author: Nathan Johnson
 * Author URI: http://boldfacedesign.com/about/nathan/
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Domain Path: /languages
 * Text Domain: boldface-testimonials
 */

 //* Don't access this file directly
 defined( 'ABSPATH' ) or die();

 //* Start bootstraping the plugin
 require( dirname( __FILE__ ) . '/includes/bootstrap.php' );
 add_action( 'plugins_loaded', array( $bootstrap = new boldface_testimonials_bootstrap( __FILE__ ), 'register' ) );

 //* Register activation and deactivation hooks
 register_activation_hook( __FILE__ , array( $bootstrap, 'activation' ) );
 register_deactivation_hook( __FILE__ , array( $bootstrap, 'deactivation' ) );
