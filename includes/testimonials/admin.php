<?php

namespace Boldface\Testimonials;

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

/**
 * Methods to handle admin pages
 *
 * @since 1.0
 */
class admin {

  /**
   * @var \Boldface\Testimonials\options
   *
   * @access public
   * @since 1.0
   */
  private $options;

  /**
   * Constructor
   *
   * @param $options \Boldface\Testimonials\options
   *
   * @access public
   * @since 1.0
   */
  public function __construct( $options ) {
    $this->options = $options;
  }

  /**
   * Add admin options page
   *
   * @access public
   * @since 1.0
   */
  public function add_options_page() {
    \add_options_page(
      __( $this->options->get( 'admin-page-title' ), 'boldface-testimonials' ), //* Page title
      __( $this->options->get( 'admin-menu-title' ), 'boldface-testimonials' ), //* Menu title
      $this->options->get( 'admin-page-capability', 'install_plugins' ),        //* Capabiilty
      $this->options->get( 'slug' ),                                            //* Menu slug
      [ $this, 'render' ]                                                       //* Callback method
    );
  }

  /**
   * Configure
   *
   * @access public
   * @since 1.0
   */
  public function configure() {

    \register_setting(
      $this->options->get( 'slug' ), //* Option group
      $this->options->get( 'slug' ), //* Option name
      [ $this, 'sanitize' ]          //* Sanitize callback method
    );

    //* Display sections
    if( $this->options->has( 'sections' ) ) {

      //* Sort sections by priority
      $sections = $this->options->get( 'sections' );
      //uasort( $sections, function( $a, $b ){
        //return $a[ 'priority' ] <=> $b[ 'priority' ];
      //} );

      foreach( $sections as $section_id => $section ) {
        \add_settings_section(
        $section_id,                                         //* Section id
        \__( $section[ 'title' ], 'boldface-testimonials' ), //* Section title
        null,                                                //* Callback method
        $this->options->get( 'slug' )                        //* Display page
      );
    }
  }

    //* Display items
    if( $this->options->has( 'items' ) ) {

      //* Sort items by priority
      $items = $this->options->get( 'items' );

      foreach( $items as $item_id ) {
        $item = $this->options->get( $item_id );
        \add_settings_field(
          $this->options->get( 'slug' ) . '_' . $item_id,   //* Field id
          \__( $item[ 'title' ], 'boldface-testimonials' ), //* Field title
          [ $this, 'item' ],                                //* Callback method
          $this->options->get( 'slug' ),                    //* Display page id
          $item[ 'section' ],                               //* Display section id
          $item                                             //* Arguments
        );
      }
    }
  }

  /**
   * Render form
   *
   * @access public
   * @since 1.0
   */
  public function render() {
    \wp_enqueue_style( $this->options->get( 'text-domain' ), $this->options->get( 'dir' ) . '/css/style.css' );
    ?>
    <div class="wrap" id="<?php echo $this->options->get( 'text-domain' );?>-admin">
      <h2><?php \_e( $this->options->get( 'admin-page-title' ), $this->options->get( 'text-domain' ) );?></h2>
      <form method="post" action="options.php">
        <?php \settings_fields( $this->options->get( 'slug' ) ); ?>
        <?php \do_settings_sections( $this->options->get( 'slug' ) ); ?>
        <?php \submit_button(); ?>
      </form>
    </div>
    <?php
  }

  /**
   * Sanitize input
   *
   * @param array $input
   * @access public
   * @since 1.0
   *
   * @return array
   */
  public function sanitize( array $input ) : array {
    foreach( $input as $key => $value ){
      $new[ $key ] = \sanitize_text_field( $value );
    }
    return $new;
  }

  /**
   * Print section description
   *
   * @access public
   * @since 1.0
   */
  public function section() {
  }

  /**
   * Print item
   *
   * @access public
   * @since 1.0
   *
   * @return null
   */
  public function item( array $args = [] ) {

    //* Nothing to do if 'slug' isn't set
    if( ! isset( $args[ 'slug' ] ) ){
      return;
    }

    //* Set default if not already set
    if( ! isset( $args[ 'default' ] ) ){
      $args[ 'default' ] = null;
    }

    //* Arguments needed for each item
    $args[ 'id' ] = sprintf( '%1$s_%2$s', $this->options->get( 'slug' ), $args[ 'slug' ] );
    $args[ 'name' ] = sprintf( '%1$s[%2$s]', $this->options->get( 'slug' ), $args[ 'slug' ] );
    $args[ 'value' ] = $this->options->get( $args[ 'slug' ], $args[ 'default' ] );

    //* Use custom callback if set as argument
    if( isset( $args[ 'callback' ] ) && is_callable( $args[ 'callback' ] ) ){
      if( ! isset( $args[ 'callback-args' ] ) ){
        $args[ 'callback-args' ] = [];
      }
      $return = call_user_func_array( \sanitize_text_field( $args[ 'callback'] ), $args[ 'callback-args' ] );
    }
    //* Try to use the default method based on argument 'type' if it exists
    elseif( $args[ 'type' ] && method_exists( $this, $args[ 'type' ] ) ){
      $this->{ $args[ 'type' ] }( $args );
    }
  }

  /**
   * Print number item
   *
   * @param array $args
   *
   * @access private
   * @since 1.0
   */
  private function number( array $args ) {
    ?>
    <input id="<?php echo $args[ 'id' ]; ?>" name="<?php echo $args[ 'name' ]; ?>" type="<?php echo $args[ 'type' ];?>" value="<?php echo $args[ 'value' ]; ?>" />
    <?php
  }

  /**
   * Print toggle - a special class of radio with two options
   *
   * @param array $args
   *
   * @access private
   * @since 1.0
   */
  private function toggle( array $args ) {
    if( ! isset( $args[ 'options' ] ) || ! is_array( $args[ 'options' ] ) || 2 !== count( $args[ 'options' ] ) ) {
      return;
    }
    if( isset( $args[ 'class' ] ) ) {
      $args[ 'class' ] .= ' toggle';
    }
    else {
      $args[ 'class' ] = 'toggle';
    }
    $this->radio( $args );
  }

  /**
   * Print radio
   *
   * @param array $args
   *
   * @access private
   * @since 1.0
   */
  private function radio( array $args ){
    if( ! isset( $args[ 'options' ] ) || ! is_array( $args[ 'options' ] ) ) {
      return;
    }
    foreach( $args[ 'options' ] as $value => $name ):
      $id = $args[ 'id' ] . '_' . $value;
    ?>
    <input id="<?php echo $id; ?>" class="<?php echo $args[ 'class' ]; ?>" type="radio" name="<?php echo $args[ 'name' ]; ?>" value="<?php echo $value; ?>"<?php if( $args['value'] == $value ){ echo ' checked'; }?> >
    <label for="<?php echo $id; ?>"><?php echo $name; ?></label>
    <?php
    endforeach;
  }

  /**
   * Print textarea
   *
   * @param array $args
   *
   * @access private
   * @since 1.0
   */
  private function textarea( array $args ) {
    ?>
    <textarea id="<?php echo $args[ 'id' ]; ?>" name="<?php echo $args[ 'name' ]; ?>" rows="5" cols="50"><?php echo $args[ 'value' ]; ?></textarea>
    <?php
  }
}
