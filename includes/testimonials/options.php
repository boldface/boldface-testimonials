<?php

namespace Boldface\Testimonials;

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

/**
 * Methods for managing the plugin options
 *
 * @since 1.0
 */
class options {

  /**
   * Options array
   *
   * @access private
   * @since 1.0
   */
  private $options;

  /**
   * Constructor
   *
   * @access public
   * @since 1.0
   */
  public function __construct() {
    $this->options = \get_option( 'boldface_testimonials', [] );
  }

  /**
   * Load options
   *
   * @access public
   * @since 1.0
   */
  public static function load() : options {
    $options = \get_option( 'boldface_testimonials', [] );

    return new self( $options );
  }

  /**
   * Has an option
   *
   * @access public
   * @since 1.0
   *
   * @return bool
   */
  public function has( $name ) {
    return isset( $this->options[ $name ] );
  }

  /**
   * Get an option
   *
   * @access public
   * @since 1.0
   *
   * @return mixed
   */
  public function get( $name, $default = null ) {
    if( ! $this->has( $name ) ) {
      return $default;
    }
    return $this->options[ $name ];
  }

  /**
   * Set an option
   *
   * @access public
   * @since 1.0
   */
  public function set( $name, $value ) {
    $this->options[ $name ] = $value;
  }

  /**
   * Add to an option
   *
   * @access public
   * @since 1.0
   */
  public function add( $name, array $arg ) {

    $var = $this->get( $name, [] );
    if( ! is_array( $var ) ){ return; }

    foreach( $arg as $key => $value ) {
      if( isset( $key ) && ! is_int( $key ) ) {
        $var[ $key ] = $value;
      }
      else{
        $var[] = $value;
      }
    }
    $this->set( $name, $var );
  }

  /**
   * Add section
   *
   * @access public
   * @since 1.0
   */
  public function add_section( array $section ) {
    if( isset( $section[ 'slug' ] ) && isset( $section[ 'title' ] ) ) {
      if( ! isset( $section[ 'priority' ] ) ) {
        $section[ 'priority' ] = 10;
      }
      $this->add( 'sections', [
        $section[ 'slug'] => $section,
      ] );
    }
  }

  /**
   * Add item
   *
   * @access public
   * @since 1.0
   */
  public function add_item( array $item ) {
    if( isset( $item[ 'slug' ] ) && isset( $item[ 'title' ] ) ) {
      $this->add( 'items', [ 'the_' . $item[ 'slug' ] ] );
      $this->add( 'the_' . $item[ 'slug' ], $item );
    }
  }
}
