<?php

namespace Boldface\Testimonials;

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

/**
 * Methods for loading the plugin
 *
 * @since 1.0
 */
class plugin {

  /**
   * @var Define all necessary plugin options
   *
   * @access private
   * @since 1.0
   */
  private $plugin_options = [
    'name'              => 'Boldface Testimonials',
    'slug'              => 'boldface_testimonials',
    'text-domain'       => 'boldface-testimonials',
    'shortcode_name'    => 'testimonial',
    'shortcode_atts'    => [
      'source' => '',
     ],
     'admin-page-title' => 'Boldface Testimonials',
     'admin-menu-title' => 'Testimonials',
   ];

  /**
   * Constructor
   *
   * @access public
   * @since 1.0
   */
  public function __construct() {
    $this->options = new options();

    //* Set plugin options
    $this->set_plugin_options();

    if( \is_admin() ) {
      $this->admin = new admin( $this->options );
    }
  }

  /*
  * Add actions to several hooks. Should be hooked from plugins_loaded
  *
  * @access public
  * @since 1.0
  */
  public static function register() {


    //* Admin stuff
    if( \is_admin() ) {
      \add_action( 'admin_menu', [ $this->admin, 'add_options_page' ] );
      \add_action( 'admin_init', [ $this->admin, 'configure' ] );
    }

    //* Shortcode
    \add_action( 'after_setup_theme', [ new shortcode( $this->options ), 'register' ] );
  }

  /**
   * Set plugin options
   *
   * @access private
   * @since 1.0
   *
   * @return \Boldface\Testimonials\options
   */
  private function set_plugin_options() {
    $this->options->set( 'path', \plugin_dir_path( __DIR__ ) );
    $this->options->set( 'dir', \plugins_url( '', __DIR__ ) );

    $this->options->add_section( [
      'slug' => 'general',
      'title' => 'General',
      'priority' => 9,
    ] );

    $this->options->add_item( [
      'slug'    => 'number',
      'title'   => 'Number',
      'type'    => 'number',
      'default' => '500',
      'section' => 'general',
    ]);

    $this->options->add_item( [
      'slug'    => 'toggle',
      'title'   => 'Toggle',
      'type'    => 'toggle',
      'default' => 'true',
      'section' => 'general',
      'options' => [ 'true' => 'True', 'false' => 'False' ],
    ] );

    foreach ( $this->plugin_options as $key => $value ) {
      $this->options->set( $key, $value );
    }
    return $this->options;
  }
}
