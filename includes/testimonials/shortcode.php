<?php

namespace Boldface\Testimonials;

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

/**
 * Methods for interacting with the WordPress shortcode API
 *
 * @since 1.0
 */
class shortcode {

  /**
   * Plugin options
   *
   * @access private
   * @since 1.0
   */
  private $options;

  /**
   * Constructor
   *
   * @param  \Boldface\Testimonials\options
   *
   * @access public
   * @since 1.0
   */
  public function __construct( $options ) {
    $this->options = $options;
  }

  /**
   * Register the shortcode
   *
   * @access public
   * @since 1.0
   */
  public function register() {
    \add_action( 'init', [ $this, 'add_shortcode' ] );
  }

  /**
   * Adds the [boldface_testimonial] shortcode
   *
   * @access public
   * @since 1.0
   */
  public function add_shortcode() {
    \add_shortcode( $this->options->get( 'shortcode_name'), [ $this, 'output' ] );
  }

  /**
   * Parse the shortcode attributes
   *
   * @access private
   * @since 1.0
   *
   * @return array $attributes
   */
  private function parse_shortcode( $attributes = [] ) {
    return $attributes = shortcode_atts( $this->options->get( 'shortcode_atts' ), $attributes );
  }

  /**
   * Output the shortcode
   *
   * @access public
   * @since 1.0
   *
   * @param array  $attributes
   * @param string $content
   *
   * @return string
   */
  public function output( $attributes = [], $content = '' ) {
    if( ! isset( $content ) ) {
      return '';
    }

    $this->attributes = $this->parse_shortcode( $attributes );

    //* Add style sheet to footer
    \wp_enqueue_style( $this->options->get( 'text-domain' ), $this->options->get( 'dir' ) . '/css/style.css' );
    ob_start();

    \do_action( 'pre_' . $this->options->get( 'slug' ) ); ?>

    <aside class="boldface testimonial">
      <div class="inner">
        <p><?php echo \apply_filters( $this->options->get( 'slug' ) . '_content', \wp_kses_post( $content ) ); ?></p>
      </div><?php
      if ( $this->attributes[ 'source' ] ): ?>
      <div class="source">
        <p><?php echo \apply_filters( $this->options->get( 'slug' ) . '_source', \wp_kses_post( $this->attributes[ 'source' ] ) );?></p>
      </div><?php
      endif; ?>
    </aside>

    <?php \do_action( 'post_' . $this->options->get( 'slug' ) );
    return \apply_filters( $this->options->get( 'slug' ), ob_get_clean() );
  }
}
